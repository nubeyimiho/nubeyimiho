
Whois Server Version 2.0

Domain names in the .com and .net domains can now be registered
with many different competing registrars. Go to http://www.internic.net
for detailed information.

   Domain Name: DRPONG.COM
   Registrar: MEGAZONE CORP. DBA HOSTING.KR
   Sponsoring Registrar IANA ID: 1489
   Whois Server: whois.hosting.kr
   Referral URL: http://HOSTING.KR
   Name Server: NS1.PARKINGCREW.NET
   Name Server: NS2.PARKINGCREW.NET
   Status: clientTransferProhibited https://icann.org/epp#clientTransferProhibited
   Updated Date: 31-aug-2016
   Creation Date: 08-aug-2014
   Expiration Date: 08-aug-2017

>>> Last update of whois database: Sat, 03 Dec 2016 07:06:25 GMT <<<

For more information on Whois status codes, please visit https://icann.org/epp

NOTICE: The expiration date displayed in this record is the date the
registrar's sponsorship of the domain name registration in the registry is
currently set to expire. This date does not necessarily reflect the expiration
date of the domain name registrant's agreement with the sponsoring
registrar.  Users may consult the sponsoring registrar's Whois database to
view the registrar's reported date of expiration for this registration.

TERMS OF USE: You are not authorized to access or query our Whois
database through the use of electronic processes that are high-volume and
automated except as reasonably necessary to register domain names or
modify existing registrations; the Data in VeriSign Global Registry
Services' ("VeriSign") Whois database is provided by VeriSign for
information purposes only, and to assist persons in obtaining information
about or related to a domain name registration record. VeriSign does not
guarantee its accuracy. By submitting a Whois query, you agree to abide
by the following terms of use: You agree that you may use this Data only
for lawful purposes and that under no circumstances will you use this Data
to: (1) allow, enable, or otherwise support the transmission of mass
unsolicited, commercial advertising or solicitations via e-mail, telephone,
or facsimile; or (2) enable high volume, automated, electronic processes
that apply to VeriSign (or its computer systems). The compilation,
repackaging, dissemination or other use of this Data is expressly
prohibited without the prior written consent of VeriSign. You agree not to
use electronic processes that are automated and high-volume to access or
query the Whois database except as reasonably necessary to register
domain names or modify existing registrations. VeriSign reserves the right
to restrict your access to the Whois database in its sole discretion to ensure
operational stability.  VeriSign may restrict or terminate your access to the
Whois database for failure to abide by these terms of use. VeriSign
reserves the right to modify these terms at any time.

The Registry database contains ONLY .COM, .NET, .EDU domains and
Registrars.
<a href="http://www.hosting.kr/servlet/bannerSvl?cmd=BANNER_CLICK&id=2" target="_blank"><img src="http://www.hosting.kr/servlet/bannerSvl?cmd=BANNER_IMAGE_VIEW&id=2" border="0"/></a>

Domain Name: drpong.com
Registry Domain ID:
Registrar WHOIS Server: whois.hosting.kr
Registrar URL: http://www.hosting.kr
Updated Date: 31-Aug-2016
Creation Date: 08-Aug-2014
Registrar Registration Expiration Date: 08-Aug-2017
Registrar: Megazone Corp., dba HOSTING.KR
Registrar IANA ID: 1489
Registrar Abuse Contact Email: help@hosting.kr
Registrar Abuse Contact Phone: +82.16447378
Reseller:
Domain Status: OK
Registry Registrant ID:
Registrant Name: noorinet
Registrant Organization: noorinet
Registrant Street: 707 Seokjang-dong Gyeongju-si
Registrant City: Gyeongsangbuk-do
Registrant State/Province:
Registrant Postal Code: 780714
Registrant Country: REPUBLIC OF KOREA
Registrant Phone: +82.1077651088
Registrant Phone Ext:
Registrant Fax: +82.547727788
Registrant Fax Ext:
Registrant Email: arirangcnt@hotmail.com
Registry Admin ID:
Admin Name: noorinet
Admin Organization: noorinet
Admin Street: 707 Seokjang-dong Gyeongju-si
Admin City: Gyeongsangbuk-do
Admin State/Province:
Admin Postal Code: 780714
Admin Country: REPUBLIC OF KOREA
Admin Phone: +82.1077651088
Admin Phone Ext:
Admin Fax: +82.547727788
Admin Fax Ext:
Admin Email: arirangcnt@hotmail.com
Registry Tech ID:
Tech Name: noorinet
Tech Organization: noorinet
Tech Street: 707 Seokjang-dong Gyeongju-si
Tech City: Gyeongsangbuk-do
Tech State/Province:
Tech Postal Code: 780714
Tech Country: REPUBLIC OF KOREA
Tech Phone: +82.1077651088
Tech Phone Ext:
Tech Fax: +82.547727788
Tech Fax Ext:
Tech Email: arirangcnt@hotmail.com
Name Server: NS1.PARKINGCREW.NET
Name Server: NS2.PARKINGCREW.NET
Name Server: 
Name Server: 
Name Server: 
DNSSEC: unsigned
URL of the ICANN WHOIS Data Problem Reporting System: http://wdprs.internic.net/

Welcome to Megazone Corp. dba hosting.kr WHOIS data service.

The data contained in hosting.kr's WhoIs database.
Megazone Corp. does not guarantee accuracy for WHOIS.
This information is provided for the sole purpose of assisting you in
obtaining information about domain name registration records.

Megazone Corp. is Domain & Hosting provider, very huge web agency Company, 
No. 1 in Korea and operating "Online Marketing & Promotion", "3D & Motion", 
"eCommerce" and "Web Portal Service". We offer all service regarding 
Information Technical and we have many experience for domain & hosting and 
web agency solution. If you want to find partner of IT industry, 
contact Megazone Corp. dba hosting.kr.

We are creating high value service and low in price for domain 
& hosting providing. Obtain good service for domain name registration, 
transfer or renewal with any hosting package with <a href="http://www.hosting.kr">http://www.hosting.kr</a>

Thank you for finding domain name of hosting.kr    
